<?php
/**
 * Implements hook_sasson_alter().
 *
 * This is how we pass variables from php to sass
 */
function sasson_sasson_alter(&$data) {
  $variables = array(
    // Grid settings
    '[grid-width]' => theme_get_setting('sasson_grid_width'),
    '[columns]' => theme_get_setting('sasson_columns'),
    '[gutter-width]' => theme_get_setting('sasson_gutter_width'),
    '[sidebar-first]' => theme_get_setting('sasson_sidebar_first'),
    '[sidebar-second]' => theme_get_setting('sasson_sidebar_second'),
    // Responsive layouts
    '[responsive]' => theme_get_setting('sasson_responsive') == 1 ? 'true' : 'false',
    '[desktop-first]' => theme_get_setting('sasson_responsive_approach') == 'desktop_first' ? 'true' : 'false',
    '[mobile-first]' => theme_get_setting('sasson_responsive_approach') == 'mobile_first' ? 'true' : 'false',
    '[narrow]' => theme_get_setting('sasson_responsive_narrow'),
    '[narrower]' => theme_get_setting('sasson_responsive_narrower'),
    '[narrowest]' => theme_get_setting('sasson_responsive_narrowest'),
    '[small]' => theme_get_setting('sasson_responsive_mf_small'),
    '[medium]' => theme_get_setting('sasson_responsive_mf_medium'),
    '[large]' => theme_get_setting('sasson_responsive_mf_large'),
    // Fonts
    '[font-face]' => theme_get_setting('sasson_font') ? theme_get_setting('sasson_font') : 'false',
    '[font-fallback]' => theme_get_setting('sasson_font_fallback') ? ', ' . theme_get_setting('sasson_font_fallback') : '',
    '[font-selectors]' => theme_get_setting('sasson_font_selectors') ? theme_get_setting('sasson_font_selectors') : 'body',
  );

  $data = str_replace(array_keys($variables), $variables, $data);
}


/**
 * Implements hook_prefixes_alter().
 *
 * This is how we define multiple vedor prefixes for our css
 */
function sasson_prefixes_alter(&$pref) {
  $pref = array(
    'border-radius' => array(
      '-moz-border-radius',
      '-webkit-border-radius',
      '-khtml-border-radius'
    ),
    'border-top-right-radius' => array(
      '-moz-border-radius-topright',
      '-webkit-border-top-right-radius',
      '-khtml-border-top-right-radius'
    ),
    'border-bottom-right-radius' => array(
      '-moz-border-radius-bottomright', 
      '-webkit-border-bottom-right-radius',
      '-khtml-border-bottom-right-radius'
    ),
    'border-bottom-left-radius' => array(
      '-moz-border-radius-bottomleft',
      '-webkit-border-bottom-left-radius',
      '-khtml-border-bottom-left-radius'
    ),
    'border-top-left-radius' => array(
      '-moz-border-radius-topleft',
      '-webkit-border-top-left-radius',
      '-khtml-border-top-left-radius'
    ),
    'box-shadow' => array(
      '-moz-box-shadow', 
      '-webkit-box-shadow'
    ),
    'box-sizing' => array(
      '-moz-box-sizing', 
      '-webkit-box-sizing'
    ),
    'opacity' => array(
      '-moz-opacity', 
      '-webkit-opacity', 
      '-khtml-opacity'
    ),
    'transition' => array(
      '-webkit-transition', 
      '-moz-transition', 
      '-ms-transition', 
      '-o-transition'
    ),
    'transform' => array(
      '-webkit-transform', 
      '-moz-transform', 
      '-ms-transform', 
      '-o-transform'
    ),
  );
}

