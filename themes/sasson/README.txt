CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Installation
 * Useful Information
 * Known Issues
 * Authors
 * Sponsors


INTRODUCTION
-----------------------

Sasson is a base theme intended for advanced drupal theming, aiming at bringing the fun back to theming.
it is a collection of many open source goodies combined together.
it was started as a fork of Zentropy (http://drupal.org/project/zentropy) as 965 (http://drupal.org/project/ninesixtyfive) but have since evolved into new areas.
most of the ideas here came from many super talented developers, credits is left where credit is due.
the bugs are mine.


FEATURES
---------------

 * It includes Sass & Compass framework - no extra requirements, just write sass/scss (thanks peroxide - https://github.com/codeincarnate/peroxide and SASSy - http://drupal.org/project/sassy)
 * It's mobile friendly - with responsive, content-first layout, out of the box. optional mobile-first responsive layout, media queries break-points are configurable.
 * It converts the core template files to HTML5 markup (thanks Boron - http://drupal.org/project/boron)
 * It includes a perfectly semantic grid system (based on 960gs http://960.gs/ via Compass - http://compass-style.org).
 * It is responsive out-of-the-box - with responsive layout and menus
 * It includes an HTML5-friendly CSS Reset (normalize), cross-browser styling compatibility improvements and other tweaks & best practices from HTML5Boilerplate v2.0 (http://html5boilerplate.com)
 * It enables HTML5 in oldIEs via HTML5shiv (http://code.google.com/p/html5shiv)
 * It *doesn't* give you a pile of CSS rules you will have to override.


MORE FEATURES
---------------

 * Adaptive grid - on your theme settings you may choose width, # of columns, gutter width, we (well, SASS) do the math.
 * Ready made sub-theme. just copy, rename, and start theming
 * Google web-fonts support (http://www.google.com/webfonts)
 * FireSASS support (https://addons.mozilla.org/en-US/firefox/addon/firesass-for-firebug)
 * HTML5 doctype and meta content-type
 * Header and Footer sections marked up with header and footer elements
 * Navigation marked up with nav elements
 * Sidebars marked up with aside elements
 * Nodes marked up with article elements containing header and footer elements
 * Comments marked up as articles nested within their parent node article
 * Blocks marked up with section elements
 * Search form uses the new input type="search" attribute
 * WAI-ARIA accessibility roles added to primary elements
 * Many extra body and node classes for easy theming
 * Responsive menus (thanks to https://github.com/joggink/jquerymobiledropdown)
 * Optional blueprint grid system integration, no more vendor prefixes, simple IE fixes, and many more - all thanks to compass (http://compass-style.org/blog/2011/04/24/v011-release/)
 * Grid background "image", for easy element aligning, made with CSS3 and SASS to fit every grid you can imagine. 
 * Draggable overlay image you can lay over your HTML for easy visual comparison. you may also set different overlay opacity values.
 * oh, and RTL support, of course.


INSTALLATION
----------------------

 * Bad way - Extract the theme in your sites/all/themes/ directory, enable it and start hacking

 * Good way - 

      * Extract the theme in your sites/all/themes/ directory
      * Move SUBTHEME into its own folder in your themes directory
      * Optional but recommended - Rename at least your folder and .info file
      * Enable your sub-theme and start hacking

 * Even better - you can use drush to create your sub-theme(s) - 

      # drush sns "My theme"


USEFUL INFORMATION
----------------------------------

 * Out of the box Sasson will give you a 960 pixel grid system, you may change grid properties in your theme settings

 * Sasson gives you a responsive layout - that means your site adapts itself to the browser it is displayed on. you may set the layout breakpoints or disable this behaviour via theme settings
 
 * The default responsive layout takes a desktop-first approach, you can go mobile-first with a click in your theme settings.
 
 * While you develop, you should keep the development mode turned on (see theme settings page), this will compile your SASS on every page load, and will give you FireSASS support (https://addons.mozilla.org/en-US/firefox/addon/firesass-for-firebug/). 

 * When not developing, turn development mode off, this will keep your CSS output light as a feather, in fact, the output of our semantic version of 960gs is much slimmer then the original css grid system.

 * Sasson allows you to write CSS3 properties (like 'border-radius', 'box-shadow' etc.) in the standard form, vendor specific prefixes will be added for you. see hook_prefixes_alter() if you want to add more.

 * Sasson passes variables from the theme settings form and into the sass compiler, you can do this in your sub-theme as well, see hook_sasson_alter().

 * Sasson will force latest IE rendering engine (even in intranet) & Chrome Frame, you may disable that via theme settings

 * Sasson will set mobile viewport initial scale to 100%. with a responsive layout, this will give your mobile users the best experience, no need to zoom on every page load, you may disable that via theme settings
 
 * sass/scss files are compiled to css files with the same name, when manually creating multiple sub-themes, you should avoid having two sass/scss files with the same name because they will override each other, if using drush sns to create sub-theme we take care of that for you.
 
 * When loading style-sheets in your .info file Sasson allows you to specify settings like media queries, browsers, weight and any option available to drupal_add_css(), for example :

		styles[styles/sasson.scss][options][weight] = 1
		styles[styles/sasson.scss][options][media] = screen and (max-width:400px)
		styles[styles/sasson.scss][options][browsers][IE] = lte IE 7
		styles[styles/sasson.scss][options][browsers][!IE] = FALSE

 This will load sasson.scss with an extra weight for screen only (not print) on browsers wider then 400px and on IE7 or older only, you get the point.

 * Sasson applies classes to the <html> tag to enable easier styling for Internet Exporer :

   - html.ie9 #selector { ... } /* IE9 only rules */
   - htm.lte-ie8 #selector { ... } /* IE8 and below rules */


KNOWN ISSUES
------------------------

* @extend doesn't respect parent selectors in nested rules - http://drupal.org/node/1388344


AUTHORS
--------------

* Tsachi Shlidor (tsi) - http://drupal.org/user/322980 & http://rtl-themes.co.il
* Raz konforti (konforti) - http://drupal.org/user/99548
* And many others...


SPONSORS
----------------

This project is made possible by :

* Linnovate (http://linnovate.net)
