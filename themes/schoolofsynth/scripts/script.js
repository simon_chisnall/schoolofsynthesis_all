/* 
 * ##### Sasson - advanced drupal theming. #####
 *   
 * SITENAME scripts.
 * 
 */

(function($) {
  
// DUPLICATE AND UNCOMMENT
//Drupal.behaviors.behaviorName = {
//  attach: function(context) {
//    // Do some magic...
//  }
//};

Drupal.behaviors.behaviorName = {
 attach: function(context) {
     
    //set main menu li widths - not used as set in css
    var email = ['i','n','f','o','@','s','c','h','o','o','l','o','f','s','y','n','t','h','e','s','i','s','.','c','o','m'];
    
	var phone = ['(','0','3',')',' ','9','8','2','7',' ','5','0','2','8'];
     $('#contact-page-details img').hover(function(){
        $('#contact-page-details').html('phone: '+ phone.join('')+'<br>email: <a href="mailto:'+email.join('')+'">'+email.join('')+'</a></p>');
      });
    
    $('.hider').each(function(){
        $(this).children('p:not(p:first)').hide();
        $(this).children('.hider_switch').click(function(){
            
            if($(this).text() == 'view more'){
                $(this).parent('.hider').children('p').slideDown();
               $(this).text('view less'); 
            }else{
                $(this).parent('.hider').children('p:not(p:first)').slideUp();
                $(this).text('view more'); 
            }
            return false;
        });
    });
    
    $('.hider-faq').each(function(){
        $(this).children('span').hide();
        $(this).children('b').click(function(){
           
                $(this).parent('.hider-faq').children('span').slideToggle() ();
            
        });
    });
    
    //Used on course display
    if($('.course-version-table').length > 0){
        
        //runs on every course select (drop down or in table) - checks for 0 stock , if so show full
        if($('.big-text .field-item').html() == '0'){
            $('.region-sidebar-first .full').show();
        }else{
            $('.region-sidebar-first .full').hide();
        }
        
        
        $('.course-version-table table a').click(function(){
            
            //set tr colour
            table = $(this).parents('table');
            $(table).find('tr').removeClass('views-row-first');
            
            $(this).parents('tr').addClass('views-row-first');
            //set drop down
            product_term = $(this).attr('href');
          $("#sidebar-first .form-item-attributes-field-product-course-term select").val(product_term);
          $("#sidebar-first .form-item-attributes-field-product-course-term select").change();
            return false;
        });
    }

    if($('.commerce-add-to-cart select').length > 0){
        
        $('.commerce-add-to-cart select').change(function(){
            
            if($(this).val()){
                
            pos = $(this).val();
            $('.course-version-table table tr').removeClass('views-row-first');
            $('.course-version-table table a[href="'+pos+'"]').parents('tr').addClass('views-row-first');
            
            }
        });
        
    }

    stock = $('.commerce-product-field-commerce-stock .field-item');
    if(stock[0]){
        stock = $(stock).html();
        if(stock < 10 && stock.indexOf("0") != 0){
            $('.commerce-product-field-commerce-stock .field-item').html('0'+stock);
        }
    }
    
    
 }
};



})(jQuery);
