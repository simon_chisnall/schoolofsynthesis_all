<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <?php if ($user_picture || !$page || $display_submitted): ?>
    <header>
      <?php print $user_picture; ?>

      <?php print render($title_prefix); ?>
      <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($display_submitted): ?>
        
      <p class="submitted">
        <?php print $submitted; ?>
        <time pubdate datetime="<?php print $submitted_pubdate; ?>">
        <?php print $submitted_date; ?>
        </time>
      </p>
        
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <span>
      <?php print render($node->body[$node->language][0]['safe_summary']);  ?>
    </span>
    <?php
      // We hide the comments, tags and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
      hide($content['field_advanced_page_extra_images']);
      
     $gallery_images = field_get_items('node', $node, 'field_advanced_page_extra_images');
   
      $body =  render($content);
      
      $gallery = '<div id="slideshow">
	&nbsp;</div>
<div id="caption">
	&nbsp;</div>
<div class="clearfix" id="thumbs">
	<ul class="thumbs noscript">';
     
      
      foreach($gallery_images as $gallery_image ){
		$img = image_style_url('about_gallery', $gallery_image['uri']);
                
          $gallery .= '<li><a class="thumb" href="'.$img.'" name="optionalCustomIdentifier" title="'.$gallery_image['alt'].'"><img alt="'.$gallery_image['alt'].'" height="90" src="'.$img.'" width="148" /></a><div class="caption">'.$gallery_image['alt'].'</div></li>';
      }
                        
	$gallery .= '</ul></div>';
      
      print str_replace('[equipment-gallery]', $gallery, $body);
      
    ?>
  </div><!-- /.content -->

 

  <?php //print render($content['comments']); ?>

</article><!-- /.node -->