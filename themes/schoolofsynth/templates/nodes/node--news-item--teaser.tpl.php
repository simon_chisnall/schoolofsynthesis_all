<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
global $base_url;
?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <a name="<?php print $title; ?>"></a>
    <?php if ($user_picture || !$page || $display_submitted): ?>
        <header>
            <?php print $user_picture; ?>
            <a href="<?php print $node_url; ?>">
                <?php print render($title_prefix); ?>
                <?php if (!$page): ?>
                    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
            </a>
        </header>
    <?php endif; ?>

    <div class="content"<?php print $content_attributes; ?>>

        <div class='blog_sm blog_sm_teaser'>
            <p class="submitted">

                <?php print $date; ?>

            </p>
            <?php
            global $base_url;
            ?>
            <!-- AddToAny BEGIN -->
            <a class="a2a_dd" href="http://www.addtoany.com/share_save?linkurl=<?php print $base_url . $node_url; ?>&amp;linkname=">Share</a>
            <script type="text/javascript">
                var a2a_config = a2a_config || {};
                a2a_config.linkurl = "<?php print $base_url . $node_url; ?>";
                a2a_config.num_services = 4;
                a2a_config.color_main = "D7E5ED";
                a2a_config.color_border = "AECADB";
                a2a_config.color_link_text = "333333";
                a2a_config.color_link_text_hover = "333333";
                a2a_config.prioritize = ["facebook", "google_plus", "twitter", "stumbleupon"];
            </script>
            <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
<?php /*
  <iframe src="//www.facebook.com/plugins/like.php?href=https://schoolofsynthesis.com<?php print urlencode($node_url);?>&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=193631007375360" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;float:left;display:block" allowTransparency="true"></iframe>

  <a href="https://twitter.com/share" class="twitter-share-button" data-via="SoSynthesis" data-url="https://schoolofsynthesis.com<?php print $node_url;?>">Tweet</a>
  <script>!function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
  if (!d.getElementById(id)) {
  js = d.createElement(s);
  js.id = id;
  js.src = p + '://platform.twitter.com/widgets.js';
  fjs.parentNode.insertBefore(js, fjs);
  }
  }(document, 'script', 'twitter-wjs');</script>

  <?php */ ?>
            <?php
            $cc = $node->comment_count;
            $s = $cc == 1 ? '' : 's';
            if($cc > 0){
                 print "<a class='post-header-comment' href='" . $node_url . "#comments'><span class='comments'>Comment$s &nbsp;$cc</span></a>";
            }
           
            ?>
        </div>

<?php
// We hide the comments, tags and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

if (isset($content['field_news_item_video'])) {
    print $content['field_news_item_video']['#items'][0]['value'];
} else {
    print render($content['field_news_item_images']);
}
?>

        <?php
        print render($content['body']);
        ?>
        <a href="<?php print $node_url; ?>">Continue reading &nldr;</a> 
    </div><!-- /.content -->

<?php /* if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
  <?php print render($content['field_tags']); ?>
  <?php print render($content['links']); ?>
  </footer>
  <?php endif; */ ?>


    <br/>
</article><!-- /.node -->
