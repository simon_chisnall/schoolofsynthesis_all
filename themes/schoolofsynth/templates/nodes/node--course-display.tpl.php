<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<div id="main">


  <article id="node-<?php print $node->nid; ?>"
           class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php print "<h1>$title</h1>"; ?>
    <?php
    print theme('image_style', array(
      'style_name' => 'scale_600x350',
      'path' => $content['field_course_display_images']['#items'][0]['uri']
    ));
    ?>
    <?php
    $products = field_get_items('node', $node, 'field_course_display_course');
    $products_array = array();
    foreach ($products as $product) {
      $products_array[] = $product['product_id'];
    }


    print views_embed_view('course_terms', 'default', implode(",", $products_array));

    print render($content['body']);

    $rcourses = render($content['field_p_d_related_courses']);

    $rposts = render($content['field_c_d_related_posts']);
    ?>
    <div id="related-courses" class="related">

      <?php
      $rcourses = render($content['field_p_d_related_courses']);
      print str_replace('Related Courses:', 'Related Courses', $rcourses);
      ?>
    </div>

    <div id="related-posts" class="related">
      <?php print str_replace('Related Blog Posts:', 'Related Blog Posts', $rposts); ?>
    </div>

  </article>

</div><!-- /#main -->

<?php
if ($content['product:field_product_is_event'][0]) {

  $extra_class = 'event-sidebar';
}
?>
<aside id="sidebar-first" role="complementary"
       class="course-sidebar <?php print $extra_class; ?>">
  <div class="region region-sidebar-first">
    <?php
    $stock = $content['product:commerce_stock'][0]['#markup'];
    $rendered_stock = render($content['product:commerce_stock']);

    $coming_soon = (boolean) $content['product:field_p_course_comming_soon']['#items'][0]['value'];

    if ($coming_soon) {
      ?>
      <img src="/<?php print path_to_theme() . "/images/comming_soon.png"; ?>"/>
    <?php }
    else { ?>
      <div class="big-text">
        <?php print $rendered_stock; ?>
        <strong>Spaces Left</strong>

        <div class="field field-name-field-course-display-course full"><span>FULL</span>
        </div>

      </div>


    <?php
    }
    if (!$coming_soon) {
      ?>
      <?php
      $addtocart = $content['field_course_display_course'];


      $addtocart = render($addtocart);


      print $addtocart;
      ?>
    <?php }
    else { ?>
      <div class="field field-name-field-course-display-course full">
        <span>FULL</span></div>
    <?php } ?>
    <div class='stats'>
      <?php
      print render($content['product:field_product_course_code']);
      print render($content['product:field_product_tutor']);

      ?>


      <?php
      print render($content['product:field_product_course_days']);
      print render($content['product:field_product_course_start_time']);
      print render($content['product:field_product_course_end_time']);
      ?>

      <?php
      if ($coming_soon) {
        $content['product:field_product_course_start_date'][0]['#markup'] = 'TBA';
        $content['product:field_product_course_end_date'][0]['#markup'] = 'TBA';
      }

      print render($content['product:field_product_course_start_date']);
      print render($content['product:field_product_course_end_date']);

      if ($coming_soon) {
        $content['product:commerce_price'][0]['#markup'] = 'TBA';
      }
      //if(!$content['product:field_product_is_event'][0]){
      print render($content['product:commerce_price']);
      //}
      ?>
    </div>
    <?php /*
          <div class="fb-like" data-send="false" data-width="250" data-show-faces="false"></div>
         */
    ?>
    <?php
    /*
      $rtutes = render($content['field_p_d_related_tutorials']);
      $rtutes = str_replace(':', '', $rtutes);
      print $rtutes;

     */

    //testimonal view
    if (isset($content['field_c_display_testimonials'])) {


      echo "<div class='course-display-testimonials'>";
      echo "<h2>Testimonials</h2>";
      print render($content['field_c_display_testimonials']);
      echo "</div>";
    }

    $blocks = block_get_blocks_by_region('sidebar_first');
    print render($blocks);
    ?>

  </div>


</aside><!-- /#sidebar-first -->


