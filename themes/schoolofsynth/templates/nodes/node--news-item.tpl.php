<?php
global $base_url;
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>




    <div class="content"<?php print $content_attributes; ?>>

        <div class='blog_sm blog_sm_teaser'>
            <p class="submitted">

                <?php print $date; ?>

            </p>
            <!-- AddToAny BEGIN -->
            <a class="a2a_dd" href="http://www.addtoany.com/share_save?linkurl=<?php print $base_url . $node_url; ?>&amp;linkname=">Share</a>
            <script type="text/javascript">
                var a2a_config = a2a_config || {};
                a2a_config.linkurl = "<?php print $base_url . $node_url; ?>";
                a2a_config.num_services = 4;
                a2a_config.color_main = "D7E5ED";
                a2a_config.color_border = "AECADB";
                a2a_config.color_link_text = "333333";
                a2a_config.color_link_text_hover = "333333";
                a2a_config.prioritize = ["facebook", "google_plus", "twitter", "stumbleupon"];
            </script>
            <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
            <?php /*
              <iframe src="//www.facebook.com/plugins/like.php?href=https://schoolofsynthesis.com<?php print urlencode($node_url);?>&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=193631007375360" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;float:left;display:block" allowTransparency="true"></iframe>

              <a href="https://twitter.com/share" class="twitter-share-button" data-via="SoSynthesis">Tweet</a>
              <script>!function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
              if (!d.getElementById(id)) {
              js = d.createElement(s);
              js.id = id;
              js.src = p + '://platform.twitter.com/widgets.js';
              fjs.parentNode.insertBefore(js, fjs);
              }
              }(document, 'script', 'twitter-wjs');</script>

              <?php
              $cc = $node->comment_count;
              $s == 1 ? '' : 's';
              print "<a class='post-header-comment' href='".$node_url."#comments'><span class='comments'>Comments $cc$s</span></a>";
             */ ?>
             <?php
            $cc = $node->comment_count;
            $s = $cc == 1 ? '' : 's';
            if($cc > 0){
                 print "<a class='post-header-comment' href='" . $node_url . "#comments'><span class='comments'>Comments &nbsp;$cc$s</span></a>";
            }
           
            ?>
        </div>

        <?php
        // We hide the comments, tags and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);

        if (isset($content['field_news_item_video'])) {
            print $content['field_news_item_video']['#items'][0]['value'];
        } else {
            print render($content['field_news_item_images']);
        }
        ?>

        <?php
        print render($content['body']);
        ?>

    </div><!-- /.content -->



    <?php print render($content['comments']); ?>
    <?php if (isset($content['field_blog_related_posts'])) : ?>
        <div class='related' id="related-posts">

            <?php
            $rposts = render($content['field_blog_related_posts']);
            print str_replace('Related Blog Posts:', 'Related Blog Posts', $rposts);
            ?>
        </div>
<?php endif; ?>
 <?php if (isset($content['field_blog_related_courses'])) : ?>
    <div class='related' id="related-courses">

        <?php
        $rcourses = render($content['field_blog_related_courses']);
        print str_replace('Related Courses:', 'Related Courses', $rcourses);
        ?>

    </div>
<?php endif; ?>


</article><!-- /.node -->

