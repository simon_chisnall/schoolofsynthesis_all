<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<a name="<?php print $title; ?>"></a>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> 
  
    <div class="course-ctas">
      <?php 
      $coming_soon = (boolean) $content['product:field_p_course_comming_soon']['#items'][0]['value'];
      $title = $node->title;
       $stock = $content['product:commerce_stock'][0]['#markup'];
      if($coming_soon || $stock == 0 ){
          if($stock == 0){ print "<span class='full'>Full</span>"; }elseif($coming_soon){?>
              <img src="/<?php print path_to_theme() . "/images/comming_soon_small.png"; ?>" /> 
       <?php   }
        //print "<a class='ce-small'  href='/course-enquiry?course=$title'>Enquire</a>"; 
      }else{
       
       print render($content['field_course_display_course']);
       
      }
        ?>
  </div>  
  
  <a class="head-link" href="<?php print $node_url; ?>">
   <?php  print "<h2>$title</h2>";?>
  </a>
   <a class="" href="<?php print $node_url; ?>"><?php
     print theme('image_style', array('style_name' => 'tutorials_page_340_200', 'path' => $content['field_course_display_images']['#items'][0]['uri'] ));
    
     ?></a>
  
 <?php
 

    print render($content['product:field_product_course_code']);
    print render($content['product:field_product_tutor']);
    ?>
    <div class="field field-type-text field-label-inline clearfix">
        <div class="field-label">Day/time:&nbsp;</div>
        <div class="field-items">
            <div class="field-item even">

                <?php
                
                if($coming_soon) { print "TBA"; }
                else{
                
                  $i = 1;
                  foreach($content['product:field_product_course_days']['#items'] as $day){
                  print $day['value'];
                  if($i != count($content['product:field_product_course_days']['#items'] )){
                  print ", ";
                  }
                  $i++;
                  }
                  print "<br/>";
                  $start =  explode(' ',$content['product:field_product_course_start_time']['#items'][0]['value']);
                  print $start[1];
                  print " to ";

                  $end =  explode(' ',$content['product:field_product_course_end_time']['#items'][0]['value']);
                  print $end[1];

                }
                ?>


            </div>

        </div>

    </div>
    <?php
    
    if($coming_soon) { $content['product:field_product_course_start_date'][0]['#markup'] = 'TBA'; $content['product:field_product_course_end_date'][0]['#markup'] = 'TBA'; }
                
    $start = render($content['product:field_product_course_start_date']);
    
    print str_replace('Start', 'Next start', $start);
            
    $end = render($content['product:field_product_course_end_date']);
    print str_replace('End', 'Next end', $end);
    
     if($coming_soon) {$content['product:commerce_price'][0]['#markup'] = 'TBA';}
    
    print render($content['product:commerce_price']);

  print render($content['body']); 
  
  ?>
    <a class="foot-link" href="<?php print $node_url; ?>">
   Full course details and further dates>
  </a>
    
  </article>


