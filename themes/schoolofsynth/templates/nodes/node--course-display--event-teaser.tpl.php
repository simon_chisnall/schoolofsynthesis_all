<article id="course-display-teaser" class="event-display <?php print $classes; ?> clearfix"<?php print $attributes; ?>> 
        <h4><a href='<?php print $node_url;?>'> <?php print $title; ?></a></h4>
        <?php
        
        $start_date = $content['product:field_product_course_start_date'][0]['#markup'];
        $start_time = $content['product:field_product_course_start_time'][0]['#markup'];
        $end_time =  $content['product:field_product_course_end_time'][0]['#markup'];
        print $start_date.' | ';
        print $start_time.' - '.$end_time;
 
        print render($content['field_course_display_images']);
          
    $description = field_get_items('node', $node, 'body');
    $description = isset($description[0]['safe_summary']) ? strip_tags($description[0]['safe_summary']) : strip_tags($description[0]['safe_summary']);
    
    if(strlen($description) > 120){
        $description = '<p>'.truncate($description, 120).'</p>';
    }else{
         $description = '<p>'.$description.'</p>';
    }
    
    $content['body'][0]['#markup'] = $description;
        print render($content['body']);
         $stock = $content['product:commerce_stock'][0]['#markup'];
      
        if ((int)$stock == 0) {
            print '<div class="field-name-field-course-display-course">'
            . '<span>FULL</span></div>';
        }else{
            $content['field_course_display_course'][0]['submit']['#value'] = 'ENROL';
            print render($content['field_course_display_course']);
        }
        
    print "<a class='course-click' href='$node_url'>event details and dates ></a>";
        ?>




    </article>
