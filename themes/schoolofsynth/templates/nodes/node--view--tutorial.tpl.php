<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<a name="<?php print $title; ?>"></a>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php
  $video = $content['field_tutorial_videos'];
  ?>
  <a href="<?php print $node_url; ?>">
  <?php
  if ($video) {

    print "<div class='video-box vjs-default-skin'>" . render($content['field_tutorial_videos']);

    print '<div class="vjs-big-play-button" tabindex="0"><span></span></div>';
  } else {
    print theme('image_style', array('style_name' => 'tutorials_page_340_200', 'path' => $content['field_tutorial_images']['#items'][0]['uri']));
  }
  ?>
  </a>
  <header>
    <?php print render($title_prefix); ?>
<?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  </header>


  <div class="content"<?php print $content_attributes; ?>>
<?php
// We hide the comments, tags and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);
hide($content['field_tutorial_images']);
hide($content['field_tutorial_videos']);

print render($content);
?>
    <a class="foot-link" href="<?php print $node_url; ?>">
      Full tutorial >
    </a>
  </div><!-- /.content -->

<?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
    </footer>
    <?php endif; ?>

  <?php print render($content['comments']); ?>

</article><!-- /.node -->