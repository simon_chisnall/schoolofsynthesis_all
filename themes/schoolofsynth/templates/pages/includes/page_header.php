<header id="header" role="banner">

    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
      <img src="/<?php print path_to_theme() . "/images/sos.png"; ?>" width="171" height="92" alt="<?php print t('Home'); ?>" />
    </a>

    <?php print render($page['header']); ?>
  
  </header><!-- /#header -->