<?php if ($page['footer']): ?>
    <footer id="footer" role="contentinfo">
     <?php print render($page['footer']); ?>
    </footer><!-- /#footer -->
<?php endif; ?>