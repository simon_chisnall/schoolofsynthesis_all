
<div id="page">

    <?php include 'includes/page_header.php'; ?>

    <div id="main">

        <div id="map_canvas" style="width:100%; height:390px"></div>

    </div><!-- /#main -->




    <aside id="sidebar-first" role="complementary" class="">
        
        <?php 
        
        
        $content =  render ($page['content']);
        print str_replace('<div class="region region-content">','<div class="region region-content"><h1>Contact</h1>', $content);
        ?>
  <?php print render($page['sidebar_first']); ?>


    </aside><!-- /#sidebar-first -->





    <?php include 'includes/page_footer.php'; ?>

</div><!-- /#page -->

<script type="text/javascript">
      
    function initialize() {
      /*
        geocoder = new google.maps.Geocoder();
        geocoder.geocode( {
            'address': "<?php print trim (preg_replace ('/\s+/' , ' ' , strip_tags (render ($page['content'])))); ?>"
        }, function(results, status){
    
        
            if (status == google.maps.GeocoderStatus.OK) {
            */        
                var myLatlng = new google.maps.LatLng(-37.846415,144.992193);//results[0].geometry.location;
                var myOptions = {
                    center: myLatlng,
                    zoom: 8,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("map_canvas"),
                myOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title:"School of Synthesis"
                }); 
        /*       
            }else{  
                alert('error finding lat lng from address');   
            }
        });
        */
        
      
     
    }
      
    initialize();
      
</script>