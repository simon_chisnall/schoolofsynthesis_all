<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php
if(isset($row->field_field_product_duration[0]['rendered']['#markup'])){
  print $row->field_field_product_duration[0]['rendered']['#markup'];
}else {
//$output should be in the format : start_date|end_date . This is then converted into a total duration
  $dates = explode("|", $output);
  $start_date = new DateTime($dates[0]);//strtotime($dates[0]);
  $end_date = new DateTime($dates[1]);
  $end_date->modify('+1 day');
  $duration = $interval = $end_date->diff($start_date);// $end_date - $start_date; //TODO - convert this to weeks
  $duration = ceil($duration->days / 7);
  print $duration . " weeks";
}
?>
