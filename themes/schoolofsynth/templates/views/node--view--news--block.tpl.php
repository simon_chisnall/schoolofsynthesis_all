<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <?php if ($user_picture || !$page || $display_submitted): ?>
    <header>
      <?php print $user_picture; ?>

      <?php print render($title_prefix); ?>
      <?php if (!$page): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($display_submitted): ?>
        
      
        
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments, tags and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
     hide($content['field_news_item_front_image']);
      
    $show_image = $content['field_news_item_front_image'];
    if($show_image){
      
     
      if (!empty($node->field_news_item_images['und'][0]['filename'])) {
        echo theme('image_style', array('style_name' => 'thumbnail', 'path' => file_build_uri($node->field_news_item_images['und'][0]['filename'])));
      }
      
    }
    
    hide($content['field_news_item_images']);
      print render($content);
    ?>
    
  </div><!-- /.content -->

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article><!-- /.node -->
