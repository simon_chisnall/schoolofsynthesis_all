<?php
$c = $variables['count'];
$r = $variables['view']->result;
$color = $r[$c]->field_field_homepageslide_text_color;
$color = $color[0]['raw']['value'];
$upcoming_slide = $r[$c]->field_field_homepageslide_upcoming;


if($color == '#ffffff '){
  $class = 'white-text';
 
}else {
   $class = 'black-text';
}

?>
<div id="views_slideshow_cycle_div_<?php print $variables['vss_id']; ?>_<?php print $variables['count']; ?>" class="<?php print $classes." ".$class; ?>">
  
<?php 

if(count($r[$variables['count']]->field_field_homepageslide_link) > 0){
    $link = $r[$variables['count']]->field_field_homepageslide_link;
    $link = $link[0]['raw']['url'];
    print "<a href='$link' class='homepageslide_link'>";
}



print $rendered_items; 

if(count($r[$variables['count']]->field_field_homepageslide_link) > 0){
  print  '</a>';
}

if($upcoming_slide[0]['raw']['value']){
  echo '<div id="upcoming-courses">';
  print views_embed_view('courses', 'block_1'); 
  echo '</div>';
}

?>
</div>
