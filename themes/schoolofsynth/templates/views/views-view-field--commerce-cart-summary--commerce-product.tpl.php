<?php
$product_id = $row->field_commerce_product[0]['raw']['product_id'];
$product = commerce_product_load($product_id);

$tutor_tid = $product->field_product_tutor['und'][0]['tid'];
$tutor = taxonomy_term_load($tutor_tid);

$stock = $product->commerce_stock['und'][0]['value'];
if ($stock >= 0 && $stock < 10) {
  $stock = '0' . $stock;
}

$day_fields = $product->field_product_course_days['und'];
$days = array();
foreach ($day_fields as $day_field) {
  $days[] = $day_field['value'];
}
$days = implode(',', $days);


?>
<div id="stock-large"><span
    class="big-text"><?php print $stock; ?></span><strong>Spaces Left</strong>
</div>
<div class='stats'>
  <?php
  print "<div id='course-detail-large'><ul><li><strong>Course title:</strong> {$product->title}</li>";
  print "<li><strong>Course code:</strong> {$product->field_product_course_code['und'][0]['value']}</li>";
  print "<li><strong>Tutor:</strong> {$tutor->name} </li>";
  print "<li><strong>Days:</strong> $days</li>";
  print "<li><strong>Start time:</strong> " . date('H:i', strtotime($product->field_product_course_start_time['und'][0]['value'])) . "</li>";
  print "<li><strong>End time:</strong> " . date('H:i', strtotime($product->field_product_course_end_time['und'][0]['value'])) . "</li>";
  print "<li><strong>Start date:</strong> " . date('d M Y', strtotime($product->field_product_course_start_date['und'][0]['value'])) . "</li>";
  print "<li><strong>End date:</strong> " . date('d M Y', strtotime($product->field_product_course_end_date['und'][0]['value'])) . "</li>";

  $price = str_replace('AUD', '', commerce_currency_format($product->commerce_price['und'][0]['amount'], ''));
  print "<li><strong>Course fee:</strong> $price</li>";
  print "</ul></div>";

  ?>
</div>
  
  
