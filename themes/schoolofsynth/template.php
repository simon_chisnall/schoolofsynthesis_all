<?php

function schoolofsynth_preprocess_node(&$variables) {
  if ($variables['node_url']) {
    $url = $variables['node_url'];
    $variables['alias'] = $url;
    $url = str_replace('/', '_', $url);
    $url = str_replace('-', '_', $url);
    $variables['theme_hook_suggestions'][] = 'node_' . $url;
  }
  if (isset($variables['view_mode'])) {
    $variables['theme_hook_suggestions'][] = 'node__' . $variables['node']->type . '__' . $variables['view_mode'];
    $variables['theme_hook_suggestions'][] = 'node__' . $variables['node']->nid . '__' . $variables['view_mode'];
  }

  $variables['date'] = date('d.m.y', $variables['created']);
}

function schoolofsynth_process_page(&$variables) {

  if (isset($variables['node']) && strtolower($variables['node']->title) == 'about') {
    drupal_add_js(drupal_get_path('theme', 'schoolofsynth') . '/scripts/jquery.galleriffic.js', 'file');
    drupal_add_js(drupal_get_path('theme', 'schoolofsynth') . '/scripts/jquery.opacityrollover.js', 'file');
    drupal_add_js("jQuery(document).ready(function () { var gallery = jQuery('#thumbs').galleriffic({imageContainerSel:'#slideshow',renderSSControls: false, renderNavControls:false,captionContainerSel:'#caption'});})", 'inline');
  }
  if (isset($variables['theme_hook_suggestions']) && isset($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . strtolower($variables['node']->title);
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }

  if (isset($variables['node']) && strtolower($variables['node']->title) == 'contact') {
    drupal_add_js('https://maps.google.com/maps/api/js?sensor=false', 'external');
  }

  //retitle checkout page
  if ($variables['title'] == 'Checkout') {
    $variables['title'] = 'Enrolment Details';
  }

  if ($variables['title'] == 'Shopping cart') {
    $variables['title'] = 'Selected Courses / Events';
  }
}

function schoolofsynth_preprocess_jplayer(&$vars) {
  $vars['items'][0]['filename'] = 'Download';
  $player = jplayer_sort_files($vars['items'], $vars['player_id'], $vars['mode']);
  $display_download = $vars['entity']->field_audio_display_download['und'][0]['value'];
  if ($display_download) {
    $vars['playlist'] = theme('jplayer_item_list', array('items' => $player['playlist']));
  }
  else {
    $vars['playlist'] = NULL;
  }
}

function schoolofsynth_form_alter(&$form, &$form_state, $form_id) {

  if (strpos($form_id, 'commerce_cart_add_to_cart_form') !== FALSE) {
    if (isset($form['submit']) && $form['submit']['#value'] == 'Add to cart'):
      $form['submit']['#value'] = 'ENROL';
    endif;
    $form['submit']['#attributes']['class'][] = 'ce-medium';
    $form['quantity']['#title'] = 'Quantity<span title="This field is required." class="form-required">*</span>';
    //$form['quantity']['#suffix'] = '<span class="small_grey">To change quantity in checkout please come back to this page.</span>';
  }
  elseif ($form_id == 'commerce_checkout_form_checkout') {
    $form['cart_contents']['#title'] = NULL;

    $form['commerce_coupon']['coupon_code']['#description'] = "Enter coupon code here";
    $form['buttons']['continue']['#value'] = "Continue";
    $form['buttons']['continue']['#attributes']['class'][] = 'ce-medium';
    // $form['buttons']['continue']['#attributes']['class'][] = 'dark';
    $form['buttons']['cancel'] = NULL;
  }
  elseif ($form_id == 'commerce_checkout_form_review') {


    if (isset($form['commerce_payment']['payment_details']['credit_card'])) {

      $form['commerce_payment']['payment_details']['credit_card']['owner']['#title'] = 'Name on card';
      $form['commerce_payment']['payment_details']['credit_card']['exp_month']['#title'] = 'Expiry';
      $form['commerce_payment']['payment_details']['credit_card']['code']['#title'] = 'Security code / CCV';
    }


    $form['help'] = NULL;
    $form['checkout_review']['review']['#data']['commerce_coupon'] = NULL;
    $form['checkout_review']['review']['#data']['cart_contents']['title'] = NULL;
    //$form['checkout_review']['review']['#data']['cart_contents']['data'] .= "<hr class='gold' />";
    $form['buttons']['continue']['#value'] = "Book course >";
    $form['buttons']['continue']['#attributes']['class'][] = 'ce-medium';
    unset($form['buttons']['back']['#prefix']);
    $form['buttons']['back']['#weight'] = 9;
    $form['buttons']['continue']['#weight'] = 10;
    $form['buttons']['back']['#value'] = '< Go Back';
    $form['buttons']['#suffix'] = "<p>   <strong>Our privacy, refund, data policy and ANZ eGate.</strong><br/>
            This site uses ANZ eGate™ to perform secure transactions using SSL 256 bit version 3 encryption. School of Synthesis does not store any credit card information.<br/>
                We will only use your email address to notify you of our course information. Your details will never be passed on to a third party.<br/>
                Please book carefully as no refunds are permitted once a course is booked and paid for. In the unlikely event a course is cancelled by School of Synthesis you will be issued with a full and immediate refund.
Any transfers between courses must be arranged between you and School of Synthesis. All transfers may incur a $50 administration fee.</br>
            </p>";

    if (isset($_SESSION['cc_response'])) {
      $form['commerce_payment']['#prefix'] = theme('commerce_checkout_errors_message', array(
        'label' => t('Errors on form'),
        'message' => $_SESSION['cc_response']
      ));
      unset($_SESSION['cc_response']);
    }
  }
  elseif (strstr($form_id, 'simplenews_block_form') !== FALSE) {
    $form['submit']['#attributes']['class'][] = 'ce-medium';
  }
  elseif ($form_id == 'commerce_checkout_form_payment' && $form['help']['#markup'] == '<div class="checkout-help">Please wait while you are redirected to the payment server. If nothing happens within 10 seconds, please click on the button below.</div>') { //used on paypal redirect page
    $form['help']['#markup'] = '<div class="checkout-help"><h2>' . t('Once you have finalised your order with paypal, please wait to be redirected back to the School of Synthesis website.
DO NOT close the browser.') . '</h2></div>';
  }
}

function schoolofsynth_commerce_checkout_review($variables) {
  $form = $variables['form'];

  // Turn the review data array into table rows.
  $rows = array();

  $i = 0;

  foreach ($form['#data'] as $pane_id => $data) {

    if ($data['title'] == 'Billing information') {
      // $data['data'] = $data['data'] . '<hr class="gold">';
    }
    // First add a row for the title.
    $rows[] = array(
      'data' => array(
        array('data' => $data['title'], 'colspan' => 2),
      ),
      'class' => array('pane-title', 'odd', 'pane-' . $i),
    );
    $i++;


    // Next, add the data for this particular section.
    if (is_array($data['data'])) {
      // If it's an array, treat each key / value pair as a 2 column row.
      foreach ($data['data'] as $key => $value) {
        $rows[] = array(
          'data' => array(
            array('data' => $key . ':', 'class' => array('pane-data-key')),
            array('data' => $value, 'class' => array('pane-data-value')),
          ),
          'class' => array('pane-data', 'even', 'pane-' . $i),
        );
      }
    }
    else {
      if ($data['data']) {
        $data['data'] = 'x' . $data['data'];
        $data['data'] = str_replace('x<div class="', '<div class="r' . $i . ' ', $data['data']);
      }
      // Otherwise treat it as a block of text in its own row.
      $rows[] = array(
        'data' => array(
          array(
            'data' => $data['data'],
            'colspan' => 2,
            'class' => array('pane-data-full')
          ),
        ),
        'class' => array('pane-data', 'even', 'pane-' . $i),
      );
    }
    $i++;
  }

  return theme('table', array(
    'rows' => $rows,
    'attributes' => array('class' => array('checkout-review'))
  ));
}

function schoolofsynth_commerce_currency_info_alter(&$currencies, $langcode) {
  $currencies['AUD']['symbol'] = '$';
  $currencies['AUD']['symbol_placement'] = 'before';
  $currencies['AUD']['code_placement'] = '';
}

function schoolofsynth_views_pre_render(&$view) {

  if ($view->name == 'commerce_line_item_table') {

    $view->result[0]->commerce_line_item_line_item_label = '';
  }
}

function schoolofsynth_form_element_label($variables) {
  $attributes = array();
  if ($variables['element']['#id'] == 'edit-commerce-payment-payment-method-paypal-wpscommerce-payment-paypal-wps') {
    $variables['element']['#title'] = '<strong>PayPal</strong><span thmr="thmr_64"> &nbsp;<img class="commerce-paypal-icon" typeof="foaf:Image" src="/sites/all/modules/commerce_paypal/images/paypal.gif" alt="PayPal" title="PayPal" /></span> ';
  }
  elseif (isset($variables['element']['#id']) && $variables['element']['#id'] == 'edit-commerce-payment-payment-method-commerce-sagepay-directcommerce-payment-commerce-sagepay-direct') {
    $payment_methods = array(
      'visa' => t('Visa'),
      'mastercard' => t('Mastercard'),
    );
    $icons = '';
    foreach ($payment_methods as $name => $title) {
      $variables = array(
        'path' => drupal_get_path('module', 'commerce_paypal') . '/images/' . $name . '.gif',
        'title' => $title,
        'alt' => $title,
        'attributes' => array(
          'class' => array('commerce-paypal-icon'),
        ),
      );
      $icons .= theme('image', $variables);
    }

    $variables['element']['#title'] = '<strong>Credit card</strong> &nbsp;<span>' . $icons . "</span>";
    $attributes['class'] = 'option';
  }
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);


  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif (isset($element['#title_display']) && $element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array(
    '!title' => $title,
    '!required' => $required
  )) . "</label>\n";
}

function schoolofsynth_process_node(&$variables) {


  $node = $variables['node'];


  $ogdescription = 'We offer high-end, tailored and hands-on training in sound production with access to the best facilities, equipment and working artists in the industry today.';
  $ogimage = "http://schoolofsynthesis.simonchisnall.com/sites/all/themes/schoolofsynth/images/sos.png";

  //if we are on individual product display or blog page
  $path = $variables['node_url'];
  if ($_SERVER['REQUEST_URI'] == url('node/' . $variables['nid'])) {


    if ($node->type == 'course_display') {
      $ogdescription = strip_tags(render(field_view_field('node', $node, 'body', array(
        'type' => 'text_summary_or_trimmed',
        'label' => 'hidden'
      ))));
      $ogimage = file_create_url($node->field_course_display_images['und'][0]['uri']);
    }
    elseif ($node->type == 'tutorial') {

      $ogdescription = strip_tags($node->body['und'][0]['summary']);
      $video_thumb = file_load($node->field_tutorial_videos ['und'][0]['thumbnail']);


      if ($video_thumb) {
        $image_uri = $video_thumb->uri;
      }
      else {
        $image_uri = $node->field_tutorial_images['und'][0]['uri'];
      }


      $ogimage = file_create_url($image_uri);
    }
  }

  $meta_description = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'og:description',
      'content' => $ogdescription
    )
  );

  drupal_add_html_head($meta_description, 'og:description');

  $meta_description = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'og:image',
      'content' => $ogimage
    )
  );

  drupal_add_html_head($meta_description, 'og:image');
}

/**
 * Override or insert variables into the html template.
 */
function schoolofsynth_preprocess_html(&$vars) {
  $music_titles = array('Courses', 'Events');
  if (drupal_is_front_page()) {
    $vars['head_title'] = "Music Production Courses | School Of Synthesis";
  }
  elseif (in_array(drupal_get_title(), $music_titles)) {
    $vars['head_title'] = implode(' | ', array(
      "Music Production " . drupal_get_title(),
      variable_get('site_name')
    ));
  }
}

function schoolofsynth_menu_link($vars) {
  $element = $vars['element'];
  $path = drupal_get_path_alias();

  if (strpos($path, 'blog') === 0 && $vars['element']['#theme'] == 'menu_link__main_menu' && $vars['element']['#href'] == 'blog') {

    $element['#localized_options']['attributes']['class'] = array('active');
  }

  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  // Adding a class depending on the TITLE of the link (not constant)
  $element['#attributes']['class'][] = drupal_html_id($element['#title']);
  // Adding a class depending on the ID of the link (constant)
  $element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function schoolofsynth_preprocess_comment_wrapper(&$variables) {

  $comment_count = count($variables['content']['comments']) - 2;

  $i = 0;

  foreach ($variables['content']['comments'] as $key => $value) {

    $div_open = $i == 0 ? "<div class='c-wrap'>" : "</div><div class='c-wrap'>";

    if (is_numeric($key)) {


      $current_is_comment = strpos($value['#prefix'], 'indented') !== FALSE ? FALSE : TRUE;

      if ($current_is_comment) {
        $variables['content']['comments'][$key]['#prefix'] = $div_open;
      }
      else {
        $variables['content']['comments'][$key]['#prefix'] = '<div class="c-reply">';
        $variables['content']['comments'][$key]['#suffix'] = '</div>';
      }

      if ($i == ($comment_count - 1)) {
        $variables['content']['comments'][$key]['#suffix'] = "</div>";
      }

      $i++;
    }
  }
}

function schoolofsynth_form_comment_form_alter(&$form, &$form_state, $form_id) {
  $form['actions']['submit']['#value'] = 'Comment';
  $form['actions']['submit']['#attributes']['class'][] = 'ce-medium';
  unset($form['subject']);
  $form['author']['mail']['#description'] = '(Will not be shown)';
}

function truncate($string, $len) {
  $string = substr($string, 0, $len);
  $string = substr($string, 0, strrpos($string, " "));
  return $string . '...';
}
