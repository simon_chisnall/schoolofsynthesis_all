<?php
/**
 * @file
 * sos_blog_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sos_blog_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
