<?php
/**
 * @file
 * sos_testimonial.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sos_testimonial_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'testimonial';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Testimonial';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Testimonials';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_testimonial_image']['id'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['table'] = 'field_data_field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['field'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_testimonial_image']['settings'] = array(
    'image_style' => '50x50_scale_crop',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[field_testimonial_image] <cite>[body]</cite>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Name linked */
  $handler->display->display_options['fields']['field_testimonial_name_linked']['id'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['table'] = 'field_data_field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['field'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['text'] = '- [field_testimonial_name_linked]';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['click_sort_column'] = 'url';
  /* Field: Content: Name */
  $handler->display->display_options['fields']['field_testimonial_name']['id'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['field'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['text'] = '([field_testimonial_name])';
  $handler->display->display_options['fields']['field_testimonial_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'testimonial' => 'testimonial',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_testimonial_image']['id'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['table'] = 'field_data_field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['field'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_testimonial_image']['settings'] = array(
    'image_style' => 'scale_crop_85x85',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[field_testimonial_image] <cite>[body]</cite>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Name linked */
  $handler->display->display_options['fields']['field_testimonial_name_linked']['id'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['table'] = 'field_data_field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['field'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['text'] = '- [field_testimonial_name_linked]';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['click_sort_column'] = 'url';
  /* Field: Content: Name */
  $handler->display->display_options['fields']['field_testimonial_name']['id'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['field'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['text'] = '([field_testimonial_name])';
  $handler->display->display_options['fields']['field_testimonial_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name']['element_label_colon'] = FALSE;

  /* Display: Block - Events */
  $handler = $view->new_display('block', 'Block - Events', 'block_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_testimonial_image']['id'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['table'] = 'field_data_field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['field'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_testimonial_image']['settings'] = array(
    'image_style' => 'scale_crop_85x85',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[field_testimonial_image] <cite>[body]</cite>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Name linked */
  $handler->display->display_options['fields']['field_testimonial_name_linked']['id'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['table'] = 'field_data_field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['field'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['text'] = '- [field_testimonial_name_linked]';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['click_sort_column'] = 'url';
  /* Field: Content: Name */
  $handler->display->display_options['fields']['field_testimonial_name']['id'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['field'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['text'] = '([field_testimonial_name])';
  $handler->display->display_options['fields']['field_testimonial_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'testimonial' => 'testimonial',
  );
  /* Filter criterion: Content: Display on page (field_display_on_page) */
  $handler->display->display_options['filters']['field_display_on_page_value']['id'] = 'field_display_on_page_value';
  $handler->display->display_options['filters']['field_display_on_page_value']['table'] = 'field_data_field_display_on_page';
  $handler->display->display_options['filters']['field_display_on_page_value']['field'] = 'field_display_on_page_value';
  $handler->display->display_options['filters']['field_display_on_page_value']['value'] = array(
    'Events' => 'Events',
  );

  /* Display: Block - Courses */
  $handler = $view->new_display('block', 'Block - Courses', 'block_2');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_testimonial_image']['id'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['table'] = 'field_data_field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['field'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_testimonial_image']['settings'] = array(
    'image_style' => 'scale_crop_85x85',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[field_testimonial_image] <cite>[body]</cite>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Name linked */
  $handler->display->display_options['fields']['field_testimonial_name_linked']['id'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['table'] = 'field_data_field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['field'] = 'field_testimonial_name_linked';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['alter']['text'] = '- [field_testimonial_name_linked]';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name_linked']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_testimonial_name_linked']['click_sort_column'] = 'url';
  /* Field: Content: Name */
  $handler->display->display_options['fields']['field_testimonial_name']['id'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['field'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['label'] = '';
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_name']['alter']['text'] = '([field_testimonial_name])';
  $handler->display->display_options['fields']['field_testimonial_name']['element_type'] = '0';
  $handler->display->display_options['fields']['field_testimonial_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'testimonial' => 'testimonial',
  );
  /* Filter criterion: Content: Display on page (field_display_on_page) */
  $handler->display->display_options['filters']['field_display_on_page_value']['id'] = 'field_display_on_page_value';
  $handler->display->display_options['filters']['field_display_on_page_value']['table'] = 'field_data_field_display_on_page';
  $handler->display->display_options['filters']['field_display_on_page_value']['field'] = 'field_display_on_page_value';
  $handler->display->display_options['filters']['field_display_on_page_value']['value'] = array(
    'Courses' => 'Courses',
  );
  $export['testimonial'] = $view;

  return $export;
}
