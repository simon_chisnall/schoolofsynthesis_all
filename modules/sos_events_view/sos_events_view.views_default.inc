<?php
/**
 * @file
 * sos_events_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sos_events_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Content: Referenced product */
  $handler->display->display_options['relationships']['field_course_display_course_product_id']['id'] = 'field_course_display_course_product_id';
  $handler->display->display_options['relationships']['field_course_display_course_product_id']['table'] = 'field_data_field_course_display_course';
  $handler->display->display_options['relationships']['field_course_display_course_product_id']['field'] = 'field_course_display_course_product_id';
  /* Field: Commerce Product: Images */
  $handler->display->display_options['fields']['field_product_course_images']['id'] = 'field_product_course_images';
  $handler->display->display_options['fields']['field_product_course_images']['table'] = 'field_data_field_product_course_images';
  $handler->display->display_options['fields']['field_product_course_images']['field'] = 'field_product_course_images';
  $handler->display->display_options['fields']['field_product_course_images']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['fields']['field_product_course_images']['label'] = '';
  $handler->display->display_options['fields']['field_product_course_images']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_course_images']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_product_course_images']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Commerce Product: Description */
  $handler->display->display_options['fields']['field_product_course_description']['id'] = 'field_product_course_description';
  $handler->display->display_options['fields']['field_product_course_description']['table'] = 'field_data_field_product_course_description';
  $handler->display->display_options['fields']['field_product_course_description']['field'] = 'field_product_course_description';
  $handler->display->display_options['fields']['field_product_course_description']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['fields']['field_product_course_description']['label'] = '';
  $handler->display->display_options['fields']['field_product_course_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_course_description']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['field_product_course_description']['settings'] = array(
    'trim_length' => '100',
  );
  /* Sort criterion: Content: Course weight (field_course_display_weight) */
  $handler->display->display_options['sorts']['field_course_display_weight_value']['id'] = 'field_course_display_weight_value';
  $handler->display->display_options['sorts']['field_course_display_weight_value']['table'] = 'field_data_field_course_display_weight';
  $handler->display->display_options['sorts']['field_course_display_weight_value']['field'] = 'field_course_display_weight_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'course_display' => 'course_display',
  );
  /* Filter criterion: Commerce Product: Display as event (field_product_is_event) */
  $handler->display->display_options['filters']['field_product_is_event_value']['id'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['table'] = 'field_data_field_product_is_event';
  $handler->display->display_options['filters']['field_product_is_event_value']['field'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['field_product_is_event_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Commerce Product: Status */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['status_1']['value'] = '1';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'events');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'view-courses';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'event_teaser';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = '<p>Stay tuned for up and coming events</p>
';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'course_display' => 'course_display',
  );
  /* Filter criterion: Commerce Product: Display as event (field_product_is_event) */
  $handler->display->display_options['filters']['field_product_is_event_value']['id'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['table'] = 'field_data_field_product_is_event';
  $handler->display->display_options['filters']['field_product_is_event_value']['field'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['field_product_is_event_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Commerce Product: Status */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['status_1']['value'] = '1';
  /* Filter criterion: Commerce Product: Start date (field_product_course_start_date) */
  $handler->display->display_options['filters']['field_product_course_start_date_value']['id'] = 'field_product_course_start_date_value';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['table'] = 'field_data_field_product_course_start_date';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['field'] = 'field_product_course_start_date_value';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['operator'] = '>';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['year_range'] = '-10:+10';
  $handler->display->display_options['path'] = 'events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Courses';

  /* Display: Courses page sidebar */
  $handler = $view->new_display('block', 'Courses page sidebar', 'course_page_top');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<a href="#[title]">[title]</a>';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;

  /* Display: Past events */
  $handler = $view->new_display('page', 'Past events', 'past_events');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'view-courses';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'event_teaser';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'course_display' => 'course_display',
  );
  /* Filter criterion: Commerce Product: Display as event (field_product_is_event) */
  $handler->display->display_options['filters']['field_product_is_event_value']['id'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['table'] = 'field_data_field_product_is_event';
  $handler->display->display_options['filters']['field_product_is_event_value']['field'] = 'field_product_is_event_value';
  $handler->display->display_options['filters']['field_product_is_event_value']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['field_product_is_event_value']['value'] = array(
    1 => '1',
  );
  /* Filter criterion: Commerce Product: Status */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['status_1']['value'] = '1';
  /* Filter criterion: Commerce Product: Start date (field_product_course_start_date) */
  $handler->display->display_options['filters']['field_product_course_start_date_value']['id'] = 'field_product_course_start_date_value';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['table'] = 'field_data_field_product_course_start_date';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['field'] = 'field_product_course_start_date_value';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['relationship'] = 'field_course_display_course_product_id';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['operator'] = '<';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_product_course_start_date_value']['year_range'] = '-10:+10';
  $handler->display->display_options['path'] = 'events';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Courses';
  $export['events'] = $view;

  return $export;
}
