<?php
/**
 * @file
 * sos_courses_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sos_courses_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
